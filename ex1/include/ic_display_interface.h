/**!
 * @file display_interface.h
 * @author Rodrigo Peixoto (rodrigopex@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2018-11-22
 *
 * @copyright Copyright (c) ic.ufal.br 2018
 *
 */
#ifndef DISPLAY_INTERFACE
#define DISPLAY_INTERFACE

#include <logging/sys_log.h>
#include <string.h>
#include <zephyr/types.h>

#define MAX_BUFFER_SIZE 20
#define CHAR_OPEN '#'
#define CHAR_CLOSE '0'

typedef enum { IC_DISPLAY_SIZE_ATTR, IC_DISPLAY_CURSOR_POSITION_ATTR } ic_display_attr_t;

typedef enum { CLOSE, OPEN } ic_display_state_t;

typedef struct {
    u8_t *cursor_position;
    u8_t buffer[MAX_BUFFER_SIZE];
    u8_t size;
    u8_t state;
} ic_display_driver_t;

/**!
 * @brief Open function definition
 * This function fills the buffer with '#' characters up to MAX_BUFFER_SIZE - 1 position,
 * set the cursor position to 0 and allow the driver to execute read, write
 * and ioctl operations
 *
 * @param display
 * @retval int 0 if it is ok, and the error code if it occours.
 */
int ic_display_open(ic_display_driver_t *display);

/**!
 * @brief Close function
 *
 * This function clears the buffer writing '0' to all the buffer and set the cursor position to 0.
 * Read, write and ioctl operations are not allowed after the driver's communication is closed
 *
 * @param display
 * @retval int 0 if it is ok, and the error code if it occours.
 */
int ic_display_close(ic_display_driver_t *display);

/**!
 * @brief Read function
 *
 * This function read the data of the buffer from the curson on. If this operation cannot be
 * complete it must return an error code.
 *
 * @param display
 * @param data: pointer to data destination. Bytes red from buffer will be saved here. It must have
 * bytes enough to fit the data to be read.
 * @param length: amount of data to be read. PS: The cursor position + length cannot be
 * greater than size.
 *
 * @retval int 0 if it is ok, and the error code if it occours.
 */
int ic_display_read(ic_display_driver_t *display, u8_t *data, u8_t length);

/**!
 * @brief Write function
 *
 * This function can write bytes to the display. After the writing action the cursor
 * position is incremented by the length of the data written. If the buffer cannot
 * store all the data, an error must be return and the data must not be written to the
 * buffer.
 *
 * @param display
 * @param data pointer to the data to be written.
 * @param length the size of the data to be written. PS: The cursor position + length cannot be
 * greater than size.
 * @retval int 0 if it is ok, and error code if it occours.
 */
int ic_display_write(ic_display_driver_t *display, u8_t *data, u8_t length);

/**!
 * @brief Control attributes function
 *
 * This function could be used to change some attribute of the driver. See the @ic_display_attr_t
 * to check which attributes can be changed. If the attribute cannot be set you must return and
 * error code.
 *
 * @param display
 * @param attr
 * @param value
 * @retval int 0 if it is ok, and the error code if it occours.
 */
int ic_display_ioctl(ic_display_driver_t *display, ic_display_attr_t attr, u8_t value);

#endif  // !DISPLAY_INTERFACE
