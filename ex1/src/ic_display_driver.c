#include <zephyr.h>

#include "ic_display_interface.h"

void print_data(u8_t *data, int size)
{
    printk("Data: ");
    for (int i = 0; i < size; ++i) {
        printk("%c", *(data + i));
    }
    printk("\n");
}

int check_display(ic_display_driver_t *display)
{
    return display == NULL;
}

int check_attr(ic_display_attr_t attr)
{
    return !((attr == IC_DISPLAY_SIZE_ATTR) || (attr == IC_DISPLAY_CURSOR_POSITION_ATTR));
}

int check_cursor(ic_display_driver_t *display)
{
    return ((u8_t *) display->cursor_position) == NULL;
}

int check_buffer(u8_t *data)
{
    return data == NULL;
}

int cursor_idx(ic_display_driver_t *display)
{
    if (check_cursor(display)) {
        return -1;
    }
    return display->cursor_position - display->buffer;
}

void print_display(ic_display_driver_t *display)
{
    SYS_LOG_INF("Print display.\n");
    if (check_display(display)) {
        return;
    }
    printk("----------------#----------------\n");
    printk("Buffer: %s\n", display->buffer);
    printk("Cursor: %d\n", cursor_idx(display));
    printk("Size: %d\n", display->size);
    printk("State: %s\n", display->state ? "OPEN" : "CLOSE");
    printk("----------------#----------------\n");
}

int ic_display_open(ic_display_driver_t *display)
{
    SYS_LOG_INF("Open display.\n");
    // Check
    if (check_display(display)) {
        SYS_LOG_ERR("Error: EFAULT.\n");
        return -EFAULT;
    } else if (display->state == OPEN) {
        SYS_LOG_ERR("Error: EBUSY.\n");
        return -EBUSY;
    }

    // Logic
    memset(display->buffer, CHAR_OPEN, MAX_BUFFER_SIZE);
    display->cursor_position = display->buffer;
    display->state           = OPEN;
    return 0;
}

int ic_display_close(ic_display_driver_t *display)
{
    SYS_LOG_INF("Close display.\n");
    // Check
    if (check_display(display) || check_cursor(display)) {
        SYS_LOG_ERR("Error: EFAULT.\n");
        return -EFAULT;
    } else if (display->state == CLOSE) {
        SYS_LOG_ERR("Error: ENODEV.\n");
        return -ENODEV;
    }

    // Logic
    memset(display->buffer, CHAR_CLOSE, MAX_BUFFER_SIZE);
    display->cursor_position = display->buffer;
    display->state           = CLOSE;
    return 0;
}

int ic_display_read(ic_display_driver_t *display, u8_t *data, u8_t length)
{
    SYS_LOG_INF("Read display.\n");
    // Check
    if (check_display(display) || check_buffer(data) || check_cursor(display)) {
        SYS_LOG_ERR("Error: EFAULT.\n");
        return -EFAULT;
    } else if (length <= 0) {
        SYS_LOG_ERR("Error: ENOTSUP.\n");
        return -ENOTSUP;
    } else if (cursor_idx(display) + length >= display->size) {
        SYS_LOG_ERR("Error: EINVAL.\n");
        return -EINVAL;
    } else if (display->state == CLOSE) {
        SYS_LOG_ERR("Error: ENODEV.\n");
        return -ENODEV;
    }

    // Logic
    u8_t *end_cursor = display->cursor_position + length;
    while (display->cursor_position != end_cursor) {
        *(data++) = *(display->cursor_position++);
    }
    if (display->cursor_position == display->buffer + MAX_BUFFER_SIZE) {
        display->cursor_position = NULL;
    }
    return 0;
}

int ic_display_write(ic_display_driver_t *display, u8_t *data, u8_t length)
{
    SYS_LOG_INF("Write display.\n");
    // Check
    if (check_display(display) || check_buffer(data) || check_cursor(display)) {
        SYS_LOG_ERR("Error: EFAULT.\n");
        return -EFAULT;
    } else if (length <= 0) {
        SYS_LOG_ERR("Error: ENOTSUP.\n");
        return -ENOTSUP;
    } else if (cursor_idx(display) + length > display->size) {
        SYS_LOG_ERR("Error: EINVAL.\n");
        return -EINVAL;
    } else if (display->state == CLOSE) {
        SYS_LOG_ERR("Error: ENODEV.\n");
        return -ENODEV;
    }

    // Logic
    u8_t *end_cursor = display->cursor_position + length;
    while (display->cursor_position != end_cursor) {
        *(display->cursor_position++) = *(data++);
    }

    if (display->cursor_position == display->buffer + MAX_BUFFER_SIZE) {
        display->cursor_position = NULL;
    }
    return 0;
}

int ic_display_ioctl(ic_display_driver_t *display, ic_display_attr_t attr, u8_t value)
{
    SYS_LOG_INF("IOCTL display.\n");
    // Check
    if (check_display(display)) {
        SYS_LOG_ERR("Error: EFAULT.\n");
        return -EFAULT;
    } else if (check_attr(attr)) {
        SYS_LOG_ERR("Error: ENOTSUP.\n");
        return -ENOTSUP;
    } else if (attr == IC_DISPLAY_SIZE_ATTR && (value > MAX_BUFFER_SIZE || value <= 0)) {
        SYS_LOG_ERR("Error: EINVAL.\n");
        return -EINVAL;
    } else if (attr == IC_DISPLAY_CURSOR_POSITION_ATTR && (value >= display->size || value < 0)) {
        SYS_LOG_ERR("Error: EINVAL.\n");
        return -EINVAL;
    } else if (attr == IC_DISPLAY_SIZE_ATTR && (cursor_idx(display) >= value)) {
        SYS_LOG_ERR("Error: EINVAL.\n");
        return -EINVAL;
    } else if (display->state == CLOSE) {
        SYS_LOG_ERR("Error: ENODEV.\n");
        return -ENODEV;
    }

    // Logic
    if (attr == IC_DISPLAY_CURSOR_POSITION_ATTR) {
        display->cursor_position = display->buffer + value;
    } else {
        display->size = value;
    }
    return 0;
}