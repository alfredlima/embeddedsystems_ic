/**!
 * @file main.c
 * @author Rodrigo Peixoto (rodrigopex@ic.ufal.br)
 * @brief
 * @version 0.1
 * @date 2018-11-22
 *
 * @copyright Copyright (c) ic.ufal.br 2018
 *
 */

#define SYS_LOG_DOMAIN "MAIN"
#include <logging/sys_log.h>
#include <misc/printk.h>
#include <string.h>
#include <zephyr.h>

#include "ic_display_interface.h"

void test_display()
{
    SYS_LOG_INF("Test...\n");
    /*
        - Open door (Ok)
        - Change size (Ok)
        - Change size (Ok)
        - Change size (Fall)
        - Change size (Fall)
        - Read of data 20 times with size 1, 2, ..., 20 always setting cursor position 0 (All Ok)
        - Read data with size 21 with cursor position 0 (Fall)
        - Write data with size 10, 3 times (2 Ok e 1 Fall)
    */

    // Create Variables
    ic_display_driver_t display = {0};
    u8_t sizes_d[]              = {10, 20, 21, 0};
    u8_t data[MAX_BUFFER_SIZE]  = {0};

    if (ic_display_open(&display)) {
        // Erro
        printk("Error open display\n");
    }
    print_display(&display);


    for (int i = 0; i < 4; ++i) {
        if (ic_display_ioctl(&display, IC_DISPLAY_SIZE_ATTR, sizes_d[i])) {
            // Erro
            printk("Error setting new size: %d\n", sizes_d[i]);
        }
        print_display(&display);
    }
    print_data(&data, MAX_BUFFER_SIZE);

    for (int i = 0; i <= MAX_BUFFER_SIZE; ++i) {
        if (ic_display_ioctl(&display, IC_DISPLAY_CURSOR_POSITION_ATTR, 0)) {
            // Erro
            printk("Error setting new position\n");
        }
        if (ic_display_read(&display, data, i)) {
            printk("Erro with read\n");
        }
        print_data(data, MAX_BUFFER_SIZE);
    }

    for (int i = -1; i <= MAX_BUFFER_SIZE; ++i) {
        if (ic_display_ioctl(&display, IC_DISPLAY_CURSOR_POSITION_ATTR, i)) {
            // Erro
            printk("Error setting new position(%d)\n", i);
        }
    }
    ic_display_ioctl(&display, IC_DISPLAY_CURSOR_POSITION_ATTR, 0);
    print_data(data, MAX_BUFFER_SIZE);
    memset(data, '1', MAX_BUFFER_SIZE);
    int er = ic_display_write(&display, data, 10);
    printk("A0: %d\n", er);
    print_display(&display);
    print_data(data, MAX_BUFFER_SIZE);
    ic_display_ioctl(&display, IC_DISPLAY_SIZE_ATTR, 5);
    print_display(&display);
    er = ic_display_write(&display, data, 10);
    printk("A1: %d\n", er);
    er = ic_display_write(&display, data, 0);
    printk("A2: %d\n", er);
    print_display(&display);
}

int main(void)
{
    SYS_LOG_INF("Run...\n");
    test_display();
    return 0;
}
